1. Le nombre total de commentaires.

SELECT count(*) FROM Comments

2. Liste des commentaires publiés triés du plus récent au plus ancien, de façon à afficher sur une vue web la page numéro 3 où chaque page contient 20 commentaires.

SELECT text FROM Comments ORDER BY date DESC

//Pour éviter de recalculer à chaque page, on peut stocker le résultat dans un fichier csv, pour ensuite piocher les lignes qu'il nous faut :

\copy (SELECT text FROM Comments ORDER BY date DESC) to 'fichier.csv' with csv

3. La liste des utilisateurs ayant leur anniversaire aujourd’hui.

SELECT id,name,birthdate FROM Users WHERE EXTRACT(DOY FROM birthdate) = EXTRACT(DOY FROM CURRENT_DATE) 

4. La liste des musées en lien avec les points situés dans la liste numéro 3.

//Création de la table Point_List_Museum

CREATE TABLE Point_List_Museum (
  point_id INT NOT NULL,
  list_id INT NOT NULL,
  museum_id INT NOT NULL,
  PRIMARY KEY (point_id, list_id, museum_id),
  FOREIGN KEY (point_id)
    REFERENCES Points (id),
  FOREIGN KEY (list_id)
    REFERENCES Lists (id),
  FOREIGN KEY (museum_id)
    REFERENCES Museums (id)
);

//Requête :

SELECT id, name FROM Museums INNER JOIN Point_List_Museum ON id = museum_id WHERE list_id = 3

5. La liste des utilisateurs inscrits durant les 31 derniers jours.

SELECT id, name, registered FROM Users WHERE age(registered) <= '31 days'

6. La liste des utilisateurs ayant postés un point durant l’année en cours.

//Création de la table Point_User

CREATE TABLE Point_User (
  point_id INT NOT NULL,
  user_id INT NOT NULL,
  PRIMARY KEY (point_id, user_id),
  FOREIGN KEY (point_id)
    REFERENCES Points (id),
  FOREIGN KEY (user_id)
    REFERENCES Users (id)
);

//Requête :

SELECT id, name FROM Users INNER JOIN Point_User ON id = user_id INNER JOIN Points ON Point_User.point_id = point_id WHERE EXTRACT(YEAR FROM created) =  EXTRACT(YEAR FROM CURRENT_DATE)

7. La liste des vingt points les plus proches des bureaux de Henoo (5 rue Jacques Villermaux 54000 Nancy) en utilisant les fonctionnalités géométriques de Postgis.

//Latitude et longitude des bureaux de Henoo :

48.692370,6.200870

//Requête :
SELECT id, title, description,ST_Distance_Sphere(ST_MakePoint(latitude, longitude),ST_MakePoint(48.692370, 6.200870)) AS distance_m FROM Points GROUP BY distance_m ASC LIMIT 20