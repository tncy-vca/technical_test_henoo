package com.henoo.technical_test_henoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechnicalTestHenooApplication {

	public static void main(String[] args) throws Throwable {
		SpringApplication.run(TechnicalTestHenooApplication.class, args);
	}

}
